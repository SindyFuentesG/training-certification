package co.com.ias.certification.backend.products.application.domain;

public class ProductException extends RuntimeException {

	public ProductException (String message) {
		super(message);
	}

}
