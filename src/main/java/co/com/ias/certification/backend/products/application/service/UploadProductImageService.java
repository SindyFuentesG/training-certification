package co.com.ias.certification.backend.products.application.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.domain.ProductImageException;
import co.com.ias.certification.backend.products.application.domain.ProductImageName;
import co.com.ias.certification.backend.products.application.domain.ProductImageNotCreated;
import co.com.ias.certification.backend.products.application.port.in.UploadProductImageUseCase;
import co.com.ias.certification.backend.products.application.port.out.CreateProductImagePort;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import co.com.ias.certification.backend.products.application.port.out.StoreProductImagesPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class UploadProductImageService implements UploadProductImageUseCase {

	private final FindProductPort findProductPort;
	private final StoreProductImagesPort storeProductImagesPort;
	private final CreateProductImagePort createProductImagePort;

	@Override
	public Try<List<String>> process(UploadProductImageCommand command) {
		return Try.of(() -> {
			Optional<Product> productU = findProductPort.findProduct(command.getId());
			return productU.map(productCreated -> {
				List<String> fileNames = new ArrayList<>();
				command.getImages().stream().forEach(image -> {
					String name = storeProductImagesPort.storeImage(command.getId(), image).getOrElseThrow(
							() -> new ProductImageException("Some images could not be uploaded, please try again"));
					createProductImagePort
							.createProductImage(ProductImageNotCreated.builder().imageName(ProductImageName.of(name))
									.product(productCreated).build())
							.getOrElseThrow(() -> new ProductImageException(
									"Some images could not be uploaded, please try again"));
					fileNames.add(name);
				});
				return fileNames;
			}).orElseThrow(() -> new ProductException("Product not found"));
		});
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
