package co.com.ias.certification.backend.products.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductId implements NumberSerializable {

	Long value;

	public static ProductId fromNumber(Number number) {
		return new ProductId(number.longValue());
	}

	public ProductId(Long value) {
		Preconditions.checkNotNull(value, "Product id can not be null");
		Preconditions.checkArgument(value >= 1, "value of product id should be greater than 0");
		this.value = value;
	}

	@Override
	public Long valueOf() {
		return value;
	}

}
