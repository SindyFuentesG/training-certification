package co.com.ias.certification.backend.orders.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class OrderId implements NumberSerializable {

	Long value;

	public static OrderId fromNumber(Number number) {
		return new OrderId(number.longValue());
	}

	private OrderId (Long value) {
		Preconditions.checkNotNull(value, "Order id can not be null");
		Preconditions.checkArgument(value >= 1, "value of order id should be greater than 0");
		this.value = value;
	}

	@Override
	public Long valueOf() {
		return value;
	}

}
