package co.com.ias.certification.backend.products.application.domain;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.StringSerializable;

import lombok.Value;

@Value(staticConstructor = "of")
public class Description implements StringSerializable {

	String value;

	private Description(String value) {
		Preconditions.checkNotNull(value, "Description can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(value), "Description can't be blank");
		Preconditions.checkArgument(StringUtils.length(value) <= 280,
				"Description can't have more than 280 characters");
		this.value = value;
	}

	@Override
	public String valueOf() {
		return value;
	}

}
