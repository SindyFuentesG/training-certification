package co.com.ias.certification.backend.products.application.port.in;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.products.application.domain.Product;
import io.vavr.control.Try;

public interface FindAllProductsUseCase {

	List<Product> findAllProducts();

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

}
