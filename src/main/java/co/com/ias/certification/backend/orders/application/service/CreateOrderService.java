package co.com.ias.certification.backend.orders.application.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Discount;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderNotCreated;
import co.com.ias.certification.backend.orders.application.domain.OrderRequest;
import co.com.ias.certification.backend.orders.application.domain.Total;
import co.com.ias.certification.backend.orders.application.port.in.CreateOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.out.CreateOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.OrderQuantityByCustomer;
import co.com.ias.certification.backend.products.application.domain.BasePrice;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class CreateOrderService implements CreateOrderUseCase {

	private final CreateOrderPort createOrderPort;
	private final FindProductPort findProductPort;
	private final OrderQuantityByCustomer orderQuantityByCustomer;
	private static final BigDecimal DISCOUNT_PERCENTAGE = new BigDecimal(0.1);

	@Override
	public Try<Order> createOrder(CreateOrderCommand command) {
		return Try.of(() -> {
			OrderRequest order = command.getOrder();
			BigDecimal discount = new BigDecimal(0);
			List<Product> items = new ArrayList<>();
			order.getItems().stream().forEach((item -> {
				Optional<Product> productOrder = findProductPort.findProduct(item);
				Product product = productOrder.orElseThrow(
						() -> new ProductException(String.format("Product with id %s not found", item.getValue())));
				items.add(product);
			}));
			BigDecimal total = items.stream().map(Product::getBasePrice).map(BasePrice::getValue)
					.reduce(BigDecimal::add).get();
			if (orderHasDiscount(command.getCustomerId())) {
				discount = total.multiply(DISCOUNT_PERCENTAGE).setScale(3, RoundingMode.HALF_UP);
				total = total.subtract(discount);
			}
			OrderNotCreated orderNotCreated = OrderNotCreated.builder().customerId(command.getCustomerId())
					.discount(Discount.of(discount)).items(items).orderStatus(order.getOrderStatus())
					.total(Total.of(total)).build();
			return orderNotCreated;
		}).flatMap(createOrderPort::createOrder);
	}

	private boolean orderHasDiscount(CustomerId customerId) {
		Long quantityOrder = orderQuantityByCustomer.orderQuantityByCustomer(customerId) + 1L;
		return quantityOrder % 3 == 0 ? true : false;
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN") || authority.getAuthority().equals("CLIENT"))) {
				return true;
			}
			return false;
		});
	}

}
