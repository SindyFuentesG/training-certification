package co.com.ias.certification.backend.orders.application.port.in;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderRequest;
import io.vavr.control.Try;
import lombok.Value;

public interface CreateOrderUseCase {
	
	Try<Order> createOrder (CreateOrderCommand command);
	
	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);
	
	@Value(staticConstructor = "of")
	class CreateOrderCommand {
		OrderRequest order;
		CustomerId customerId;
	}

}
