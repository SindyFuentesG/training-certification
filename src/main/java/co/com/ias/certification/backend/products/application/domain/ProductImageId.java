package co.com.ias.certification.backend.products.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductImageId implements NumberSerializable{
	
	Long value;

	public static ProductImageId fromNumber(Number number) {
		return new ProductImageId(number.longValue());
	}

	public ProductImageId(Long value) {
		Preconditions.checkNotNull(value, "Image id can not be null");
		Preconditions.checkArgument(value >= 1, "value of Image Id should be greater than 0");
		this.value = value;
	}

	@Override
	public Long valueOf() {
		return value;
	}


}
