package co.com.ias.certification.backend.orders.application.port.out;

import co.com.ias.certification.backend.products.application.domain.ProductId;

public interface OrderQuantityByProductPort {

	Long orderQuantityByProduct(ProductId productId);

}
