package co.com.ias.certification.backend.products.application.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductNotCreated;
import co.com.ias.certification.backend.products.application.port.in.CreateProductUseCase;
import co.com.ias.certification.backend.products.application.port.out.CreateProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class CreateProductService implements CreateProductUseCase {

	private final CreateProductPort createProductPort;

	@Override
	public Try<Product> createProduct(CreateProductCommand command) {
		ProductNotCreated product = command.getProduct();
		return createProductPort.createProduct(product);
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
