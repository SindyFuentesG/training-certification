package co.com.ias.certification.backend.products.application.domain;

import org.springframework.core.io.Resource;

import com.google.common.base.Preconditions;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProductImage {

	ProductImageId id;
	ProductId productId;
	ProductImageName imageName;
	Resource imageResource;

	public ProductImage(ProductImageId id, ProductId productId, ProductImageName imageName, Resource imageResource) {
		this.id = Preconditions.checkNotNull(id);
		this.productId = Preconditions.checkNotNull(productId);
		this.imageName = Preconditions.checkNotNull(imageName);
		this.imageResource = imageResource;
	}

}
