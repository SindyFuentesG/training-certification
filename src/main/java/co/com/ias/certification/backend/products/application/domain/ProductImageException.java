package co.com.ias.certification.backend.products.application.domain;

public class ProductImageException extends RuntimeException {
	
	public ProductImageException (String message) {
		super(message);
	}

}
