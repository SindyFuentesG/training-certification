package co.com.ias.certification.backend.orders.application.port.in;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.orders.application.domain.Order;
import io.vavr.control.Try;

public interface FindAllOrdersUseCase {

	List<Order> findAllOrders();

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

}
