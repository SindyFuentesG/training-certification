package co.com.ias.certification.backend.orders.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderException;
import co.com.ias.certification.backend.orders.application.port.in.DeleteOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.out.DeleteOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.FindOrderPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class DeleteOrderService implements DeleteOrderUseCase {

	private final FindOrderPort findOrderPort;
	private final DeleteOrderPort deleteOrderPort;

	@Override
	public Try<Order> deleteOrder(DeleteProductCommand command) {
		return Try.of(() -> {
			Optional<Order> order = findOrderPort.findOrder(command.getId());
			return order.map(orderDelete -> {
				deleteOrderPort.deleteOrder(command.getId());
				return orderDelete;
			}).orElseThrow(() -> new OrderException("Order not found"));
		});
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
