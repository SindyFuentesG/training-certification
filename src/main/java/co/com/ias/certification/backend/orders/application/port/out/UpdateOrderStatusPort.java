package co.com.ias.certification.backend.orders.application.port.out;

import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderStatus;
import io.vavr.control.Try;

public interface UpdateOrderStatusPort {
	
	Try<Order> updateOrderStatus(Order order, OrderStatus newStatus);

}
