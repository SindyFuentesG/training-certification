package co.com.ias.certification.backend.orders.adapters.out.persitence;

import co.com.ias.certification.backend.common.PersistenceAdapter;
import co.com.ias.certification.backend.orders.application.port.out.OrderQuantityByProductPort;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class OrderItemPersistenceAdapter implements OrderQuantityByProductPort {
	
	private final OrderItemRepository orderItemRepository;
	
	@Override
	public Long orderQuantityByProduct(ProductId productId) {
		return orderItemRepository.countByProductId(productId.getValue());
	}

}
