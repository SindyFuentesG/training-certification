package co.com.ias.certification.backend.products.adapters.out.persistence;

import org.springframework.data.repository.CrudRepository;

public interface ProductImageRepository extends CrudRepository<ProductImageJpaEntity, Long> {

}
