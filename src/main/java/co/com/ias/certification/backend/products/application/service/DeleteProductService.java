package co.com.ias.certification.backend.products.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.port.out.OrderQuantityByProductPort;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.port.in.DeleteProductUseCase;
import co.com.ias.certification.backend.products.application.port.out.DeleteProductPort;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class DeleteProductService implements DeleteProductUseCase {

	private final FindProductPort findProductPort;
	private final DeleteProductPort deleteProductPort;
	private final OrderQuantityByProductPort orderQuantityByProductPort;

	@Override
	public Try<Product> deleteProduct(DeleteProductCommand command) {
		return Try.of(() -> {
			Optional<Product> product = findProductPort.findProduct(command.getId());
			if (orderQuantityByProductPort.orderQuantityByProduct(command.getId()) > 0) {
				throw new ProductException("Product has some orders associated, please remove this orders first");
			}
			return product.map(productDelete -> {
				deleteProductPort.deleteProduct(command.getId());
				return productDelete;
			}).orElseThrow(() -> new ProductException("Product not found"));
		});
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
