package co.com.ias.certification.backend.products.application.port.out;

import org.springframework.web.multipart.MultipartFile;

import co.com.ias.certification.backend.products.application.domain.ProductId;
import io.vavr.control.Try;

public interface StoreProductImagesPort {

	Try<String> storeImage(ProductId id, MultipartFile image);

}
