package co.com.ias.certification.backend.orders.application.domain;

import java.util.List;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.products.application.domain.ProductId;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class OrderRequest {

	OrderStatus orderStatus;
	List<ProductId> items;

	public OrderRequest(OrderStatus orderStatus, List<ProductId> items) {
		this.orderStatus = Preconditions.checkNotNull(orderStatus);
		Preconditions.checkNotNull(items);
		Preconditions.checkArgument(!items.isEmpty(), "List of products can not be empty");
		this.items = items;
	}

}
