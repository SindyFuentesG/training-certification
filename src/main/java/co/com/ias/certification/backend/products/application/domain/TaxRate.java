package co.com.ias.certification.backend.products.application.domain;

import java.math.BigDecimal;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class TaxRate implements NumberSerializable {

	BigDecimal value;

	public static TaxRate fromNumber(Number number) {
		return new TaxRate(BigDecimal.valueOf(number.doubleValue()));
	}

	public TaxRate(BigDecimal value) {
		Preconditions.checkNotNull(value, "Tax Rate can not be null");
		Preconditions.checkArgument(!(value.compareTo(BigDecimal.ZERO) == -1 || value.compareTo(BigDecimal.ONE) == 1),
				"The vale of tax rate should be between 0 and 1");
		this.value = value;
	}

	@Override
	public BigDecimal valueOf() {
		return value;
	}

}
