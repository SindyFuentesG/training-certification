package co.com.ias.certification.backend.products.application.port.out;

import co.com.ias.certification.backend.products.application.domain.ProductImage;
import co.com.ias.certification.backend.products.application.domain.ProductImageNotCreated;
import io.vavr.control.Try;

public interface CreateProductImagePort {
	
	Try<ProductImage> createProductImage(ProductImageNotCreated productImage);

}
