package co.com.ias.certification.backend.products.application.port.in;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.multipart.MultipartFile;

import co.com.ias.certification.backend.products.application.domain.ProductId;
import io.vavr.control.Try;
import lombok.Value;

public interface UploadProductImageUseCase {

	Try<List<String>> process(UploadProductImageCommand command);

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

	@Value(staticConstructor = "of")
	class UploadProductImageCommand {
		ProductId id;
		List<MultipartFile> images;
	}

}
