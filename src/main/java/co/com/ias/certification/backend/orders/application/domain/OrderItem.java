package co.com.ias.certification.backend.orders.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.products.application.domain.Product;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class OrderItem {

	OrderItemId id;
	Product product;

	public OrderItem(OrderItemId id, Product product) {
		this.id = Preconditions.checkNotNull(id);
		this.product = Preconditions.checkNotNull(product);
	}

}
