package co.com.ias.certification.backend.orders.adapters.out.persitence;

import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Discount;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderId;
import co.com.ias.certification.backend.orders.application.domain.OrderItem;
import co.com.ias.certification.backend.orders.application.domain.OrderItemId;
import co.com.ias.certification.backend.orders.application.domain.OrderNotCreated;
import co.com.ias.certification.backend.orders.application.domain.OrderStatus;
import co.com.ias.certification.backend.orders.application.domain.Total;
import co.com.ias.certification.backend.products.adapters.out.persistence.ProductMapper;
import co.com.ias.certification.backend.products.application.domain.Product;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class OrderMapper {

	private final ProductMapper productMapper;

	public Order mapOrderToDomainEntity(OrderJpaEntity jpaEntity) {
		Preconditions.checkNotNull(jpaEntity);
		return Order.builder().id(OrderId.of(jpaEntity.getId())).customerId(CustomerId.of(jpaEntity.getCustomerId()))
				.discount(Discount.of(jpaEntity.getDiscount())).total(Total.of(jpaEntity.getTotal()))
				.orderStatus(OrderStatus.valueOf(jpaEntity.getStatus())).items(jpaEntity.getItems().stream()
						.map(item -> this.mapOrderItemToDomainEntity(item)).collect(Collectors.toList()))
				.build();
	}

	public OrderJpaEntity mapOrderToJpaEntity(OrderNotCreated order) {
		Preconditions.checkNotNull(order);
		return OrderJpaEntity.builder().total(order.getTotal().getValue()).customerId(order.getCustomerId().getValue())
				.discount(order.getDiscount().getValue()).status(order.getOrderStatus().toString())
				.items(order.getItems().stream().map(item -> this.mapOrderItemToJpaEntity(item))
						.collect(Collectors.toList()))
				.build();
	}

	public OrderItem mapOrderItemToDomainEntity(OrderItemJpaEntity jpaEntity) {
		Preconditions.checkNotNull(jpaEntity);
		return OrderItem.builder().id(OrderItemId.of(jpaEntity.getId()))
				.product(productMapper.mapToDomainEntity(jpaEntity.getProduct())).build();
	}

	public OrderItemJpaEntity mapOrderItemToJpaEntity(Product product) {
		Preconditions.checkNotNull(product);
		return OrderItemJpaEntity.builder().product(productMapper.mapToJpaEntity(product)).build();
	}
	
	public OrderItemJpaEntity mapOrderItemToJpaEntity(OrderItem item) {
		Preconditions.checkNotNull(item);
		return OrderItemJpaEntity.builder().id(item.getId().getValue()).product(productMapper.mapToJpaEntity(item.getProduct())).build();
	}
	
	public OrderJpaEntity mapOrderToJpaEntity(Order order) {
		Preconditions.checkNotNull(order);
		return OrderJpaEntity.builder().id(order.getId().getValue()).total(order.getTotal().getValue()).customerId(order.getCustomerId().getValue())
				.discount(order.getDiscount().getValue()).status(order.getOrderStatus().toString())
				.items(order.getItems().stream().map(item -> this.mapOrderItemToJpaEntity(item))
						.collect(Collectors.toList()))
				.build();
	}	
	
}
