package co.com.ias.certification.backend.products.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class InventoryQuantity implements NumberSerializable {

	Integer value;

	public static InventoryQuantity fromNumber(Number number) {
		return new InventoryQuantity(number.intValue());
	}

	private InventoryQuantity(Integer value) {
		Preconditions.checkNotNull(value, "Quantity can not be null");
		Preconditions.checkArgument(value > 0, "Quantity must be greater than 0");
		this.value = value;
	}

	@Override
	public Integer valueOf() {
		return value;
	}

}
