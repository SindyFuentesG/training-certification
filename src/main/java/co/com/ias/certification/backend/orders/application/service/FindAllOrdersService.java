package co.com.ias.certification.backend.orders.application.service;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.port.in.FindAllOrdersUseCase;
import co.com.ias.certification.backend.orders.application.port.out.FindAllOrdersPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class FindAllOrdersService implements FindAllOrdersUseCase {

	private final FindAllOrdersPort findAllOrdersPort;

	@Override
	public List<Order> findAllOrders() {
		return findAllOrdersPort.findAllOrders();
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
