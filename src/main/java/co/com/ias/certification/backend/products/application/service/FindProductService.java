package co.com.ias.certification.backend.products.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.port.in.FindProductUseCase;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class FindProductService implements FindProductUseCase {

	private final FindProductPort findProductPort;

	@Override
	public Try<Product> findProduct(FindProductQuery query) {
		return Try.of(() -> {
			Optional<Product> product = findProductPort.findProduct(query.getId());
			return product.orElseThrow(() -> new ProductException("Product not found"));
		});
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN") || authority.getAuthority().equals("CLIENT"))) {
				return true;
			}
			return false;
		});
	}

}
