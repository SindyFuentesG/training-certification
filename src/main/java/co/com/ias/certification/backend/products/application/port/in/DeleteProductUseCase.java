package co.com.ias.certification.backend.products.application.port.in;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import io.vavr.control.Try;
import lombok.Value;

public interface DeleteProductUseCase {

	Try<Product> deleteProduct(DeleteProductCommand command);

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

	@Value(staticConstructor = "of")
	class DeleteProductCommand {
		ProductId id;
	}

}
