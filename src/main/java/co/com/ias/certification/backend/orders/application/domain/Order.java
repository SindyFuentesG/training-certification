package co.com.ias.certification.backend.orders.application.domain;

import java.util.List;

import com.google.common.base.Preconditions;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Value
@Builder
@Data
public class Order {

	OrderId id;
	Total total;
	Discount discount;
	OrderStatus orderStatus;
	CustomerId customerId;
	List<OrderItem> items;

	public Order(OrderId id, Total total, Discount discount, OrderStatus orderStatus, CustomerId customerId,
			List<OrderItem> items) {
		this.id = Preconditions.checkNotNull(id);
		this.total = Preconditions.checkNotNull(total);
		this.discount = Preconditions.checkNotNull(discount);
		this.orderStatus = Preconditions.checkNotNull(orderStatus);
		this.customerId = Preconditions.checkNotNull(customerId);
		Preconditions.checkNotNull(items);
		Preconditions.checkArgument(!items.isEmpty(), "List of products can not be empty");
		this.items = items;
	}

}
