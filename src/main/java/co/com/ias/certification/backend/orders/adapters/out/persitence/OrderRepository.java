package co.com.ias.certification.backend.orders.adapters.out.persitence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<OrderJpaEntity, Long> {
	
	Long countByCustomerId(String customerId);
	
	List<OrderJpaEntity> findByCustomerId(String customerId);
	
	
}
