package co.com.ias.certification.backend.products.application.domain;

public enum ProductStatus {

	BORRADOR, PUBLICADO;

}
