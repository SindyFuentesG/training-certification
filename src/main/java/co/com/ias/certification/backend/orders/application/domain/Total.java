package co.com.ias.certification.backend.orders.application.domain;

import java.math.BigDecimal;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class Total implements NumberSerializable {

	BigDecimal value;

	public static Total fromNumber(Number number) {
		return new Total(BigDecimal.valueOf(number.doubleValue()));
	}

	private Total(BigDecimal value) {
		Preconditions.checkNotNull(value, "Total can not be null");
		Preconditions.checkArgument(!(value.compareTo(BigDecimal.ZERO) == -1), "Total must be higher than zero");
		this.value = value;
	}

	@Override
	public BigDecimal valueOf() {
		return value;
	}

}
