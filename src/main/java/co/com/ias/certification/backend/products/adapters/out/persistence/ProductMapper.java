package co.com.ias.certification.backend.products.adapters.out.persistence;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.products.application.domain.BasePrice;
import co.com.ias.certification.backend.products.application.domain.Description;
import co.com.ias.certification.backend.products.application.domain.InventoryQuantity;
import co.com.ias.certification.backend.products.application.domain.Name;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import co.com.ias.certification.backend.products.application.domain.ProductImage;
import co.com.ias.certification.backend.products.application.domain.ProductImageId;
import co.com.ias.certification.backend.products.application.domain.ProductImageName;
import co.com.ias.certification.backend.products.application.domain.ProductImageNotCreated;
import co.com.ias.certification.backend.products.application.domain.ProductNotCreated;
import co.com.ias.certification.backend.products.application.domain.ProductStatus;
import co.com.ias.certification.backend.products.application.domain.TaxRate;
import io.vavr.control.Try;

@Component
public class ProductMapper {

	public Product mapToDomainEntity(ProductJpaEntity jpaEntity) {
		Preconditions.checkNotNull(jpaEntity);
		return Product.builder().id(ProductId.of(jpaEntity.getId())).name(Name.of(jpaEntity.getName()))
				.description(Description.of(jpaEntity.getDescription()))
				.basePrice(BasePrice.of(jpaEntity.getBasePrice())).taxRate(TaxRate.of(jpaEntity.getTaxRate()))
				.productStatus(ProductStatus.valueOf(jpaEntity.getStatus()))
				.inventoryQuantity(InventoryQuantity.of(jpaEntity.getInventoryQuantity())).images(jpaEntity.getImages()
						.stream().map(image -> this.mapProductImageToDomainEntity(image)).collect(Collectors.toList()))
				.build();
	}

	public ProductJpaEntity mapToJpaEntity(ProductNotCreated product) {
		Preconditions.checkNotNull(product);
		return ProductJpaEntity.builder().name(product.getName().getValue())
				.description(product.getDescription().getValue()).basePrice(product.getBasePrice().getValue())
				.taxRate(product.getTaxRate().getValue()).status(product.getProductStatus().toString())
				.inventoryQuantity(product.getInventoryQuantity().getValue()).build();
	}

	public ProductJpaEntity mapToJpaEntity(Product product) {
		Preconditions.checkNotNull(product);
		return ProductJpaEntity.builder().id(product.getId().getValue()).name(product.getName().getValue())
				.description(product.getDescription().getValue()).basePrice(product.getBasePrice().getValue())
				.taxRate(product.getTaxRate().getValue()).status(product.getProductStatus().toString())
				.inventoryQuantity(product.getInventoryQuantity().getValue()).build();
	}

	public ProductImage mapProductImageToDomainEntity(ProductImageJpaEntity jpaEntity) {
		return ProductImage.builder().id(ProductImageId.of(jpaEntity.getId()))
				.imageName(ProductImageName.of(jpaEntity.getName()))
				.productId(ProductId.of(jpaEntity.getProduct().getId()))
				.imageResource(this.mapImageToResource(jpaEntity.getName()).getOrNull()).build();
	}

	public ProductImageJpaEntity mapProductImageToJpaEntity(ProductImageNotCreated productImage) {
		Preconditions.checkNotNull(productImage);
		return ProductImageJpaEntity.builder().name(productImage.getImageName().getValue())
				.product(this.mapToJpaEntity(productImage.getProduct())).build();
	}

	public Try<Resource> mapImageToResource(String nameImage) {
		return Try.of(() -> {
			Path path = Paths.get("uploads").resolve(nameImage).toAbsolutePath();
			return new UrlResource(path.toUri());
		});
	}

}
