package co.com.ias.certification.backend.orders.application.port.out;

import java.util.List;

import co.com.ias.certification.backend.orders.application.domain.Order;

public interface FindAllOrdersPort {

	List<Order> findAllOrders();

}
