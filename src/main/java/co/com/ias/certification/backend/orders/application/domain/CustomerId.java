package co.com.ias.certification.backend.orders.application.domain;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.StringSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class CustomerId implements StringSerializable {

	String value;

	private CustomerId(String value) {
		Preconditions.checkNotNull(value, "Customer Id can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(value), "Customer Id can't be blank");
		this.value = value;
	}

	@Override
	public String valueOf() {
		return value;
	}

}
