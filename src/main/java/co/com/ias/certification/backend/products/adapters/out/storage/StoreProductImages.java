package co.com.ias.certification.backend.products.adapters.out.storage;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import co.com.ias.certification.backend.products.application.domain.ProductId;
import co.com.ias.certification.backend.products.application.port.out.StoreProductImagesPort;
import io.vavr.control.Try;

@Repository
public class StoreProductImages implements StoreProductImagesPort {

	private final static String DIRECTORIO_UPLOAD = "uploads";

	@Override
	public Try<String> storeImage(ProductId id, MultipartFile image) {
		return Try.of(() -> {
			String extension = FilenameUtils.getExtension(image.getOriginalFilename());
			String uuid = UUID.randomUUID().toString();
			String fileName = String.format("%s_%s.%s", id.getValue(), uuid, extension);
			Path rutaArchivo = Paths.get(DIRECTORIO_UPLOAD).resolve(fileName).toAbsolutePath();
			Files.copy(image.getInputStream(), rutaArchivo);
			return fileName;
		});
	}

}
