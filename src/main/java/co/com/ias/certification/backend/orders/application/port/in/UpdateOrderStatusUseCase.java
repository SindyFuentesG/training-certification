package co.com.ias.certification.backend.orders.application.port.in;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderId;
import co.com.ias.certification.backend.orders.application.domain.OrderStatus;
import io.vavr.control.Try;
import lombok.Value;

public interface UpdateOrderStatusUseCase {

	Try<Order> updateOrderStatus(UpdateOrderStatusCommand command);

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

	@Value(staticConstructor = "of")
	class UpdateOrderStatusCommand {
		OrderId id;
		OrderStatus orderStatus;
	}

}
