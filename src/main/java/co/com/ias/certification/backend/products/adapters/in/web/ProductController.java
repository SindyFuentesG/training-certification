package co.com.ias.certification.backend.products.adapters.in.web;

import java.util.Arrays;
import java.util.List;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.ias.certification.backend.common.WebAdapter;
import co.com.ias.certification.backend.common.exception.UnauthorizedException;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import co.com.ias.certification.backend.products.application.port.in.CreateProductUseCase;
import co.com.ias.certification.backend.products.application.port.in.DeleteProductUseCase;
import co.com.ias.certification.backend.products.application.port.in.FindAllProductsUseCase;
import co.com.ias.certification.backend.products.application.port.in.FindProductUseCase;
import co.com.ias.certification.backend.products.application.port.in.UpdateProductUseCase;
import co.com.ias.certification.backend.products.application.port.in.UploadProductImageUseCase;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
@WebAdapter
@CrossOrigin(origins = "*")
public class ProductController {

	private final CreateProductUseCase createProductUseCase;
	private final FindAllProductsUseCase findAllProductsUseCase;
	private final FindProductUseCase findProductUseCase;
	private final DeleteProductUseCase deleteProductUseCase;
	private final UpdateProductUseCase updateProductUseCase;
	private final UploadProductImageUseCase uploadProductImageUseCase;

	@PostMapping
	public ResponseEntity<?> createProduct(@RequestBody CreateProductUseCase.CreateProductCommand command,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (createProductUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			return ResponseEntity.ok(createProductUseCase.createProduct(command));
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@GetMapping("/all")
	public ResponseEntity<?> findAllProducts(Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (findAllProductsUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			return ResponseEntity.ok(findAllProductsUseCase.findAllProducts());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@GetMapping
	public ResponseEntity<?> findProductById(@RequestBody FindProductUseCase.FindProductQuery query,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (findProductUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Product> product = findProductUseCase.findProduct(query);
			if (product.isSuccess()) {
				return ResponseEntity.ok(product);
			}
			return ResponseEntity.badRequest().body(product.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@DeleteMapping
	public ResponseEntity<?> deleteProductById(@RequestBody DeleteProductUseCase.DeleteProductCommand command,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (deleteProductUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Product> productDeleted = deleteProductUseCase.deleteProduct(command);
			if (productDeleted.isSuccess()) {
				return ResponseEntity.ok(productDeleted);
			}
			return ResponseEntity.badRequest().body(productDeleted.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@PutMapping
	public ResponseEntity<?> updateById(@RequestBody UpdateProductUseCase.UpdateProductCommand command,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (updateProductUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Product> productUpdated = updateProductUseCase.updateProduct(command);
			if (productUpdated.isSuccess()) {
				return ResponseEntity.ok(productUpdated);
			}
			return ResponseEntity.badRequest().body(productUpdated.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@PostMapping("/images")
	public ResponseEntity<?> uploadProductImages(@RequestParam("file") MultipartFile[] images,
			@RequestParam("id") Long productId, Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (uploadProductImageUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			ProductId id = ProductId.of(productId);
			UploadProductImageUseCase.UploadProductImageCommand command = UploadProductImageUseCase.UploadProductImageCommand
					.of(id, Arrays.asList(images));
			Try<List<String>> nameImages = uploadProductImageUseCase.process(command);
			if (nameImages.isSuccess()) {
				return ResponseEntity.ok(nameImages);
			}
			return ResponseEntity.badRequest().body(nameImages.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

}
