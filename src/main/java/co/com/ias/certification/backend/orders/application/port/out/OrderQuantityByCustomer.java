package co.com.ias.certification.backend.orders.application.port.out;

import co.com.ias.certification.backend.orders.application.domain.CustomerId;

public interface OrderQuantityByCustomer {

	Long orderQuantityByCustomer(CustomerId customerId);

}
