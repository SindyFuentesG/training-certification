package co.com.ias.certification.backend.products.application.service;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.port.in.FindAllProductsUseCase;
import co.com.ias.certification.backend.products.application.port.out.FindAllProductsPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class FindAllProductsService implements FindAllProductsUseCase {

	private final FindAllProductsPort findAllProductsPort;

	@Override
	public List<Product> findAllProducts() {
		return findAllProductsPort.findAllProducts();
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN") || authority.getAuthority().equals("CLIENT"))) {
				return true;
			}
			return false;
		});
	}
}
