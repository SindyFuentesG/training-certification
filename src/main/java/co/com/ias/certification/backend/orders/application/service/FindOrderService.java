package co.com.ias.certification.backend.orders.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderException;
import co.com.ias.certification.backend.orders.application.port.in.FindOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.out.FindOrderPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class FindOrderService implements FindOrderUseCase {

	private final FindOrderPort findOrderPort;

	@Override
	public Try<Order> findOrder(FindOrderQuery query) {
		return Try.of(() -> {
			Optional<Order> order = findOrderPort.findOrder(query.getId());
			return order.orElseThrow(() -> new OrderException("Order not found"));
		});
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
