package co.com.ias.certification.backend.orders.adapters.out.persitence;

import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<OrderItemJpaEntity, Long> {
	
	Long countByProductId(Long productId);

}
