package co.com.ias.certification.backend.orders.adapters.in.web;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.ias.certification.backend.common.WebAdapter;
import co.com.ias.certification.backend.common.exception.UnauthorizedException;
import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderRequest;
import co.com.ias.certification.backend.orders.application.port.in.CreateOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.in.DeleteOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.in.FindAllOrdersUseCase;
import co.com.ias.certification.backend.orders.application.port.in.FindOrderUseCase;
import co.com.ias.certification.backend.orders.application.port.in.FindOrdersByUserUseCase;
import co.com.ias.certification.backend.orders.application.port.in.UpdateOrderStatusUseCase;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@WebAdapter
@CrossOrigin(origins = "*")
public class OrderController {

	private final CreateOrderUseCase createOrderUseCase;
	private final FindAllOrdersUseCase findAllOrdersUseCase;
	private final FindOrderUseCase findOrderUseCase;
	private final UpdateOrderStatusUseCase updateOrderStatusUseCase;
	private final DeleteOrderUseCase deleteOrderUseCase;
	private final FindOrdersByUserUseCase findOrdersByUserUseCase;

	@PostMapping
	public ResponseEntity<?> createOrder(@RequestBody OrderRequest order, Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		String customerId = authenticationToken.getAccount().getKeycloakSecurityContext().getToken().getPreferredUsername();
		CreateOrderUseCase.CreateOrderCommand command = CreateOrderUseCase.CreateOrderCommand.of(order,
				CustomerId.of(customerId));
		if (createOrderUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			return ResponseEntity.ok(createOrderUseCase.createOrder(command));
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@GetMapping("/all")
	public ResponseEntity<?> findAllOrders(Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (findAllOrdersUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			return ResponseEntity.ok(findAllOrdersUseCase.findAllOrders());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@GetMapping
	public ResponseEntity<?> findOrderById(@RequestBody FindOrderUseCase.FindOrderQuery query,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (findOrderUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Order> order = findOrderUseCase.findOrder(query);
			if (order.isSuccess()) {
				return ResponseEntity.ok(order);
			}
			return ResponseEntity.badRequest().body(order.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@PutMapping
	public ResponseEntity<?> updateStatusById(@RequestBody UpdateOrderStatusUseCase.UpdateOrderStatusCommand command,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (updateOrderStatusUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Order> orderUpdated = updateOrderStatusUseCase.updateOrderStatus(command);
			if (orderUpdated.isSuccess()) {
				return ResponseEntity.ok(orderUpdated);
			}
			return ResponseEntity.badRequest().body(orderUpdated.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

	@DeleteMapping
	public ResponseEntity<?> deleteOrderById(@RequestBody DeleteOrderUseCase.DeleteProductCommand command,
			Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if (deleteOrderUseCase.userHasPermission(authenticationToken.getAuthorities()).get()) {
			Try<Order> orderDeleted = deleteOrderUseCase.deleteOrder(command);
			if (orderDeleted.isSuccess()) {
				return ResponseEntity.ok(orderDeleted);
			}
			return ResponseEntity.badRequest().body(orderDeleted.getCause());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}
	
	@GetMapping("/user")
	public ResponseEntity<?> getOrdersByUser(@RequestBody FindOrdersByUserUseCase.FindOrderByUserQuery query, Authentication authentication) {
		KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) authentication;
		if(findOrdersByUserUseCase.userHasPermission(authenticationToken, query.getCustomerId()).get()) {
			return ResponseEntity.ok(findOrdersByUserUseCase.findOrdersByUser(query));
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new UnauthorizedException("You do not have permissions to do this action"));
	}

}
