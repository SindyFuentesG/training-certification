package co.com.ias.certification.backend.orders.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderException;
import co.com.ias.certification.backend.orders.application.port.in.UpdateOrderStatusUseCase;
import co.com.ias.certification.backend.orders.application.port.out.FindOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.UpdateOrderStatusPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class UpdateOrderStatusService implements UpdateOrderStatusUseCase {

	private final FindOrderPort findOrderPort;
	private final UpdateOrderStatusPort updateOrderPort;

	@Override
	public Try<Order> updateOrderStatus(UpdateOrderStatusCommand command) {
		return Try.of(() -> {
			Optional<Order> order = findOrderPort.findOrder(command.getId());
			return order.map(orderToUpdate -> {
				return orderToUpdate;
			}).orElseThrow(() -> new OrderException("Order not found"));
		}).flatMap(orderUpdate -> updateOrderPort.updateOrderStatus(orderUpdate, command.getOrderStatus()));
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
