package co.com.ias.certification.backend.products.application.port.in;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductNotCreated;
import io.vavr.control.Try;
import lombok.Value;

public interface CreateProductUseCase {

	Try<Product> createProduct(CreateProductCommand command);

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

	@Value(staticConstructor = "of")
	class CreateProductCommand {
		ProductNotCreated product;
	}

}
