package co.com.ias.certification.backend.orders.application.domain;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class OrderItemId implements NumberSerializable {

	Long value;

	public static OrderItemId fromNumber(Number number) {
		return new OrderItemId(number.longValue());
	}

	private OrderItemId(Long value) {
		Preconditions.checkNotNull(value, "Order item id can not be null");
		Preconditions.checkArgument(value >= 1, "value of order item id should be greater than 0");
		this.value = value;
	}

	@Override
	public Long valueOf() {
		return value;
	}

}
