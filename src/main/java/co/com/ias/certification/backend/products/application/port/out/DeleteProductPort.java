package co.com.ias.certification.backend.products.application.port.out;

import co.com.ias.certification.backend.products.application.domain.ProductId;

public interface DeleteProductPort {
	
	void deleteProduct (ProductId productId);

}
