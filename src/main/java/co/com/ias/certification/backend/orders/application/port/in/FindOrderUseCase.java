package co.com.ias.certification.backend.orders.application.port.in;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderId;
import io.vavr.control.Try;
import lombok.Value;

public interface FindOrderUseCase {

	Try<Order> findOrder(FindOrderQuery query);

	Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities);

	@Value(staticConstructor = "of")
	class FindOrderQuery {
		OrderId id;
	}

}
