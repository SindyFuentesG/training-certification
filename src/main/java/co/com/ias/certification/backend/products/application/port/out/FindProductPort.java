package co.com.ias.certification.backend.products.application.port.out;

import java.util.Optional;

import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductId;

public interface FindProductPort {

	Optional<Product> findProduct(ProductId productId);

}
