package co.com.ias.certification.backend.orders.application.service;

import java.util.Collection;
import java.util.List;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.port.in.FindOrdersByUserUseCase;
import co.com.ias.certification.backend.orders.application.port.out.FindOrdersByUserPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class FindOrdersByUserService implements FindOrdersByUserUseCase {

	private final FindOrdersByUserPort findOrdersByUserPort;

	@Override
	public List<Order> findOrdersByUser(FindOrderByUserQuery query) {
		return findOrdersByUserPort.findOrdersByUser(query.getCustomerId());
	}

	@Override
	public Try<Boolean> userHasPermission(KeycloakAuthenticationToken authenticationToken, CustomerId customerId) {
		Collection<GrantedAuthority> authorities = authenticationToken.getAuthorities();
		String loggedCustomer = authenticationToken.getAccount().getKeycloakSecurityContext().getToken()
				.getPreferredUsername();
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN")) || loggedCustomer.equals(customerId.getValue())) {
				return true;
			}
			return false;
		});
	}
}
