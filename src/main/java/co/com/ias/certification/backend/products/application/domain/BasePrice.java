package co.com.ias.certification.backend.products.application.domain;

import java.math.BigDecimal;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class BasePrice implements NumberSerializable {
	
	BigDecimal value;

	public static BasePrice fromNumber(Number number) {
		return new BasePrice(BigDecimal.valueOf(number.doubleValue()));
	}

	private BasePrice(BigDecimal value) {
		Preconditions.checkNotNull(value, "Base price can not be null");
		Preconditions.checkArgument(!(value.compareTo(BigDecimal.ZERO) == -1), "Price must be higher than zero");
		this.value = value;
	}

	@Override
	public BigDecimal valueOf() {
		return value;
	}

}
