package co.com.ias.certification.backend.orders.application.domain;

public class OrderException extends RuntimeException {

	public OrderException(String message) {
		super(message);
	}

}
