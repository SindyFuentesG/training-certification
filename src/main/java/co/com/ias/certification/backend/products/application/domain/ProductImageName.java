package co.com.ias.certification.backend.products.application.domain;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.StringSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductImageName implements StringSerializable {

	String value;

	private ProductImageName(String value) {
		Preconditions.checkNotNull(value, "Image name can not be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(value), "Image name can't be blank");
		this.value = value;
	}

	@Override
	public String valueOf() {
		return value;
	}

}
