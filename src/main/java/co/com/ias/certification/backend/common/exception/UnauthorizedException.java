package co.com.ias.certification.backend.common.exception;

public class UnauthorizedException extends RuntimeException {
	
	public UnauthorizedException (String message) {
		super(message);
	}

}
