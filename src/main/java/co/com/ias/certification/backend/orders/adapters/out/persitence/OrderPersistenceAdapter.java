package co.com.ias.certification.backend.orders.adapters.out.persitence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import co.com.ias.certification.backend.common.PersistenceAdapter;
import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderId;
import co.com.ias.certification.backend.orders.application.domain.OrderNotCreated;
import co.com.ias.certification.backend.orders.application.domain.OrderStatus;
import co.com.ias.certification.backend.orders.application.port.out.CreateOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.DeleteOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.FindAllOrdersPort;
import co.com.ias.certification.backend.orders.application.port.out.FindOrderPort;
import co.com.ias.certification.backend.orders.application.port.out.FindOrdersByUserPort;
import co.com.ias.certification.backend.orders.application.port.out.OrderQuantityByCustomer;
import co.com.ias.certification.backend.orders.application.port.out.UpdateOrderStatusPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class OrderPersistenceAdapter implements CreateOrderPort, FindAllOrdersPort, OrderQuantityByCustomer,
		FindOrderPort, UpdateOrderStatusPort, DeleteOrderPort, FindOrdersByUserPort {

	private final OrderRepository orderRepository;
	private final OrderMapper orderMapper;

	@Override
	public Try<Order> createOrder(OrderNotCreated order) {
		return Try.of(() -> {
			OrderJpaEntity orderJpaEntity = orderMapper.mapOrderToJpaEntity(order);
			OrderJpaEntity createdOrder = orderRepository.save(orderJpaEntity);
			return orderMapper.mapOrderToDomainEntity(createdOrder);
		});
	}

	@Override
	public List<Order> findAllOrders() {
		List<OrderJpaEntity> orders = (List<OrderJpaEntity>) orderRepository.findAll();
		return orders.stream().map(order -> orderMapper.mapOrderToDomainEntity(order)).collect(Collectors.toList());
	}

	@Override
	public Long orderQuantityByCustomer(CustomerId customerId) {
		return orderRepository.countByCustomerId(customerId.getValue());
	}

	@Override
	public Optional<Order> findOrder(OrderId orderId) {
		return orderRepository.findById(orderId.getValue()).map(orderJpaEntity -> {
			return orderMapper.mapOrderToDomainEntity(orderJpaEntity);
		});
	}

	@Override
	public Try<Order> updateOrderStatus(Order order, OrderStatus newStatus) {
		return Try.of(() -> {
			OrderJpaEntity orderJpaEntity = orderMapper.mapOrderToJpaEntity(order);
			orderJpaEntity.setStatus(newStatus.toString());
			OrderJpaEntity updatedOrder = orderRepository.save(orderJpaEntity);
			return orderMapper.mapOrderToDomainEntity(updatedOrder);
		});
	}

	@Override
	public void deleteOrder(OrderId orderId) {
		orderRepository.deleteById(orderId.getValue());
	}

	@Override
	public List<Order> findOrdersByUser(CustomerId customerId) {
		List<OrderJpaEntity> orders = (List<OrderJpaEntity>) orderRepository.findByCustomerId(customerId.getValue());
		return orders.stream().map(order -> orderMapper.mapOrderToDomainEntity(order)).collect(Collectors.toList());
	}

}
