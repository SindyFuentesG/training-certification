package co.com.ias.certification.backend.products.application.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.common.UseCase;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.port.in.UpdateProductUseCase;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import co.com.ias.certification.backend.products.application.port.out.UpdateProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class UpdateProductService implements UpdateProductUseCase {

	private final FindProductPort findProductPort;
	private final UpdateProductPort updateProductPort;

	@Override
	public Try<Product> updateProduct(UpdateProductCommand command) {
		return Try.of(() -> {
			Optional<Product> productU = findProductPort.findProduct(command.getId());
			return productU.map(productCreated -> {
				return productCreated;
			}).orElseThrow(() -> new ProductException("Product not found"));
		}).flatMap(productUpdated -> updateProductPort.updateProduct(command.getId(), command.getProduct()));
	}

	@Override
	public Try<Boolean> userHasPermission(Collection<GrantedAuthority> authorities) {
		return Try.of(() -> {
			if (authorities.stream().anyMatch(authority -> authority.getAuthority().equals("EMPLOYEE")
					|| authority.getAuthority().equals("ADMIN"))) {
				return true;
			}
			return false;
		});
	}

}
