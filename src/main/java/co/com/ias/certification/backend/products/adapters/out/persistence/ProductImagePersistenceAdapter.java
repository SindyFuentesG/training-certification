package co.com.ias.certification.backend.products.adapters.out.persistence;

import co.com.ias.certification.backend.common.PersistenceAdapter;
import co.com.ias.certification.backend.products.application.domain.ProductImage;
import co.com.ias.certification.backend.products.application.domain.ProductImageNotCreated;
import co.com.ias.certification.backend.products.application.port.out.CreateProductImagePort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class ProductImagePersistenceAdapter implements CreateProductImagePort {

	private final ProductImageRepository productImageRepository;
	private final ProductMapper productMapper;

	@Override
	public Try<ProductImage> createProductImage(ProductImageNotCreated productImage) {
		return Try.of(() -> {
			ProductImageJpaEntity productImageJpaEntity = productMapper.mapProductImageToJpaEntity(productImage);
			ProductImageJpaEntity createdProductImage = productImageRepository.save(productImageJpaEntity);
			return productMapper.mapProductImageToDomainEntity(createdProductImage);
		});
	}

}
