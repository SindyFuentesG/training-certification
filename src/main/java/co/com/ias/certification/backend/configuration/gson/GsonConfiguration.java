package co.com.ias.certification.backend.configuration.gson;

import java.lang.reflect.Type;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import co.com.ias.certification.backend.common.exception.UnauthorizedException;
import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Discount;
import co.com.ias.certification.backend.orders.application.domain.OrderException;
import co.com.ias.certification.backend.orders.application.domain.OrderId;
import co.com.ias.certification.backend.orders.application.domain.OrderItemId;
import co.com.ias.certification.backend.orders.application.domain.Total;
import co.com.ias.certification.backend.products.application.domain.BasePrice;
import co.com.ias.certification.backend.products.application.domain.Description;
import co.com.ias.certification.backend.products.application.domain.InventoryQuantity;
import co.com.ias.certification.backend.products.application.domain.Name;
import co.com.ias.certification.backend.products.application.domain.ProductException;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import co.com.ias.certification.backend.products.application.domain.ProductImageException;
import co.com.ias.certification.backend.products.application.domain.ProductImageId;
import co.com.ias.certification.backend.products.application.domain.ProductImageName;
import co.com.ias.certification.backend.products.application.domain.TaxRate;
import co.com.ias.certification.backend.serialization.NumberValueAdapter;
import co.com.ias.certification.backend.serialization.StringValueAdapter;
import co.com.ias.certification.backend.serialization.TryAdapter;
import io.vavr.control.Try;

@Configuration
public class GsonConfiguration {

	@Bean
	public Gson gson() {
		return new GsonBuilder().registerTypeAdapter(Name.class, new StringValueAdapter<>(Name::of))
				.registerTypeAdapter(Description.class, new StringValueAdapter<>(Description::of))
				.registerTypeAdapter(BasePrice.class, new NumberValueAdapter<>(BasePrice::fromNumber))
				.registerTypeAdapter(ProductId.class, new NumberValueAdapter<>(ProductId::fromNumber))
				.registerTypeAdapter(InventoryQuantity.class, new NumberValueAdapter<>(InventoryQuantity::fromNumber))
				.registerTypeAdapter(TaxRate.class, new NumberValueAdapter<>(TaxRate::fromNumber))
				.registerTypeAdapter(ProductImageId.class, new NumberValueAdapter<>(ProductImageId::fromNumber))
				.registerTypeAdapter(ProductImageName.class, new StringValueAdapter<>(ProductImageName::of))
				.registerTypeAdapter(OrderId.class, new NumberValueAdapter<>(OrderId::fromNumber))
				.registerTypeAdapter(CustomerId.class, new StringValueAdapter<>(CustomerId::of))
				.registerTypeAdapter(Discount.class, new NumberValueAdapter<>(Discount::fromNumber))
				.registerTypeAdapter(Total.class, new NumberValueAdapter<>(Total::fromNumber))
				.registerTypeAdapter(OrderItemId.class, new NumberValueAdapter<>(OrderItemId::fromNumber))
				.registerTypeAdapter(UnauthorizedException.class, new JsonSerializer<UnauthorizedException>() {
					@Override
					public JsonElement serialize(UnauthorizedException src, Type typeOfSrc,
							JsonSerializationContext context) {
						JsonObject jsonObject = new JsonObject();
						String message = src.getMessage();
						JsonPrimitive errorValue = new JsonPrimitive(message);
						jsonObject.add("error", errorValue);
						return jsonObject;
					}
				}).registerTypeAdapter(ProductException.class, new JsonSerializer<ProductException>() {
					@Override
					public JsonElement serialize(ProductException src, Type typeOfSrc,
							JsonSerializationContext context) {
						JsonObject jsonObject = new JsonObject();
						String message = src.getMessage();
						JsonPrimitive errorValue = new JsonPrimitive(message);
						jsonObject.add("error", errorValue);
						return jsonObject;
					}
				}).registerTypeAdapter(ProductImageException.class, new JsonSerializer<ProductImageException>() {
					@Override
					public JsonElement serialize(ProductImageException src, Type typeOfSrc,
							JsonSerializationContext context) {
						JsonObject jsonObject = new JsonObject();
						String message = src.getMessage();
						JsonPrimitive errorValue = new JsonPrimitive(message);
						jsonObject.add("error", errorValue);
						return jsonObject;
					}
				}).registerTypeAdapter(OrderException.class, new JsonSerializer<OrderException>() {
					@Override
					public JsonElement serialize(OrderException src, Type typeOfSrc, JsonSerializationContext context) {
						JsonObject jsonObject = new JsonObject();
						String message = src.getMessage();
						JsonPrimitive errorValue = new JsonPrimitive(message);
						jsonObject.add("error", errorValue);
						return jsonObject;
					}
				}).registerTypeAdapter(Try.class, new TryAdapter<>()).create();
	}

}
