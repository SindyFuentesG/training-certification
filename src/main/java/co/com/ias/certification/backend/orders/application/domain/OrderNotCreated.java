package co.com.ias.certification.backend.orders.application.domain;

import java.util.List;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.products.application.domain.Product;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class OrderNotCreated {

	Total total;
	Discount discount;
	OrderStatus orderStatus;
	CustomerId customerId;
	List<Product> items;

	public OrderNotCreated(Total total, Discount discount, OrderStatus orderStatus, CustomerId customerId,
			List<Product> items) {
		this.total = Preconditions.checkNotNull(total);
		this.discount = Preconditions.checkNotNull(discount);
		this.orderStatus = Preconditions.checkNotNull(orderStatus);
		this.customerId = Preconditions.checkNotNull(customerId);
		Preconditions.checkNotNull(items);
		Preconditions.checkArgument(!items.isEmpty(), "List of products can not be empty");
		this.items = items;
	}

}
