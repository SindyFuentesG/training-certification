package co.com.ias.certification.backend.orders.application.domain;

import java.math.BigDecimal;

import com.google.common.base.Preconditions;

import co.com.ias.certification.backend.serialization.NumberSerializable;
import lombok.Value;

@Value(staticConstructor = "of")
public class Discount implements NumberSerializable {
	
	BigDecimal value;
	
	public static Discount fromNumber(Number number) {
		return new Discount(BigDecimal.valueOf(number.doubleValue()));
	}
	
	private Discount (BigDecimal value) {
		Preconditions.checkNotNull(value, "Discount can not be null");
		Preconditions.checkArgument(!(value.compareTo(BigDecimal.ZERO) == -1), "Discount must be higher than zero");
		this.value = value;
	}
	
	@Override
	public BigDecimal valueOf() {
		return value;
	}

}
