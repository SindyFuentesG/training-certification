package co.com.ias.certification.backend.products.application.domain;

import com.google.common.base.Preconditions;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProductImageNotCreated {
	ProductImageName imageName;
	Product product;

	public ProductImageNotCreated(ProductImageName imageName, Product product) {
		this.product = Preconditions.checkNotNull(product);
		this.imageName = Preconditions.checkNotNull(imageName);
	}

}
