package co.com.ias.certification.backend.orders.application.port.out;

import java.util.Optional;

import co.com.ias.certification.backend.orders.application.domain.Order;
import co.com.ias.certification.backend.orders.application.domain.OrderId;

public interface FindOrderPort {

	Optional<Order> findOrder(OrderId orderId);

}
