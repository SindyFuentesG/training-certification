package co.com.ias.certification.backend.orders.application.port.in;

import java.util.Collection;
import java.util.List;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import co.com.ias.certification.backend.orders.application.domain.CustomerId;
import co.com.ias.certification.backend.orders.application.domain.Order;
import io.vavr.control.Try;
import lombok.Value;

public interface FindOrdersByUserUseCase {
	
	List<Order> findOrdersByUser(FindOrderByUserQuery query);
	
	Try<Boolean> userHasPermission(KeycloakAuthenticationToken authenticationToken, CustomerId customerId);
	
	@Value(staticConstructor = "of")
	class FindOrderByUserQuery {
		CustomerId customerId;
	}

}
