package co.com.ias.certification.backend.products.application.port.out;

import java.util.List;

import co.com.ias.certification.backend.products.application.domain.Product;

public interface FindAllProductsPort {
	
	List<Product> findAllProducts();

}
