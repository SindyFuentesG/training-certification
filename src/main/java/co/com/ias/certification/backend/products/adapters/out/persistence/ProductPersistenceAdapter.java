package co.com.ias.certification.backend.products.adapters.out.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import co.com.ias.certification.backend.common.PersistenceAdapter;
import co.com.ias.certification.backend.products.application.domain.Product;
import co.com.ias.certification.backend.products.application.domain.ProductId;
import co.com.ias.certification.backend.products.application.domain.ProductNotCreated;
import co.com.ias.certification.backend.products.application.port.out.CreateProductPort;
import co.com.ias.certification.backend.products.application.port.out.DeleteProductPort;
import co.com.ias.certification.backend.products.application.port.out.FindAllProductsPort;
import co.com.ias.certification.backend.products.application.port.out.FindProductPort;
import co.com.ias.certification.backend.products.application.port.out.UpdateProductPort;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class ProductPersistenceAdapter
		implements CreateProductPort, FindAllProductsPort, FindProductPort, DeleteProductPort, UpdateProductPort {

	private final ProductRepository productRepository;
	private final ProductMapper productMapper;

	@Override
	public Try<Product> createProduct(ProductNotCreated product) {
		return Try.of(() -> {
			ProductJpaEntity productJpaEntity = productMapper.mapToJpaEntity(product);
			ProductJpaEntity createdProduct = productRepository.save(productJpaEntity);
			return productMapper.mapToDomainEntity(createdProduct);
		});
	}

	@Override
	public List<Product> findAllProducts() {
		List<ProductJpaEntity> products = (List<ProductJpaEntity>) productRepository.findAll();
		return products.stream().map(product -> productMapper.mapToDomainEntity(product)).collect(Collectors.toList());
	}

	@Override
	public Optional<Product> findProduct(ProductId productId) {
		return productRepository.findById(productId.getValue()).map(productJpaEntity -> {
			return productMapper.mapToDomainEntity(productJpaEntity);
		});
	}

	@Override
	public void deleteProduct(ProductId productId) {
		productRepository.deleteById(productId.getValue());
	}

	@Override
	public Try<Product> updateProduct(ProductId productId, ProductNotCreated product) {
		return Try.of(() -> {
			ProductJpaEntity productJpaEntity = productMapper.mapToJpaEntity(product);
			productJpaEntity.setId(productId.getValue());
			ProductJpaEntity updatedProduct = productRepository.save(productJpaEntity);
			return productMapper.mapToDomainEntity(updatedProduct);
		});
	}

}
