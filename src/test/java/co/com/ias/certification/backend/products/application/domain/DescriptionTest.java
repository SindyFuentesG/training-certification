package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class DescriptionTest {
	
	@Test
	void of() {
	}
	
	@TestFactory
	@DisplayName("Debería crear descripciones validas")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of("Computador portatil", "Teclado inalambrico", "Mouse inalambrico").map(description -> {
			String testDescription = String.format("Debería ser valido para la descripción: %s ", description);
			Executable executable = () -> {
				ThrowingSupplier<Description> descriptionThrowingSupplier = () -> Description.of(description);
				assertAll(() -> assertDoesNotThrow(descriptionThrowingSupplier),
						() -> assertNotNull(descriptionThrowingSupplier.get()),
						() -> assertEquals(descriptionThrowingSupplier.get().getValue(), description));
			};
			return DynamicTest.dynamicTest(testDescription, executable);
		});
	}

	@TestFactory
	@DisplayName("No debería crear descripciones")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of("",
				"Texto de prueba para descripciones de productos muy largas, \r\n" + 
				"Texto de prueba para descripciones de productos muy largas, \r\n" + 
				"Texto de prueba para descripciones de productos muy largas, \r\n" + 
				"Texto de prueba para descripciones de productos muy largas,\r\n" + 
				"Texto de prueba para descripciones de productos muy largas",
				"    ").map(description -> {
					String testDescription = String.format("Debería ser invalido para el name %s", description);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> Description.of(description)));
					};
					return DynamicTest.dynamicTest(testDescription, executable);
				});
	}
	
	@Test
	@DisplayName("No debería crear descripciones para valores envíados como null")
	void itShouldNotPassNull() {
		String description = null;
		assertThrows(NullPointerException.class, () -> Description.of(description));
	}

}
