package co.com.ias.certification.backend.serialization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Value;

class NumberValueAdapterTest {

	static Gson gson;

	@Test
	void of() {
	}

	@Value(staticConstructor = "of")
	public static class BigDecimalAdapterTest implements NumberSerializable {
		BigDecimal value;

		public static BigDecimalAdapterTest fromNumber(Number number) {
			return new BigDecimalAdapterTest(BigDecimal.valueOf(number.doubleValue()));
		}

		@Override
		public BigDecimal valueOf() {
			return value;
		}
	}

	@Value(staticConstructor = "of")
	public static class LongAdapterTest implements NumberSerializable {
		Long value;

		public static LongAdapterTest fromNumber(Number number) {
			return new LongAdapterTest(number.longValue());
		}

		@Override
		public Long valueOf() {
			return value;
		}
	}

	@Value(staticConstructor = "of")
	public static class IntegerAdapterTest implements NumberSerializable {
		Integer value;

		public static IntegerAdapterTest fromNumber(Number number) {
			return new IntegerAdapterTest(number.intValue());
		}

		@Override
		public Integer valueOf() {
			return value;
		}
	}

	@BeforeAll
	static void setUp() {
		gson = new GsonBuilder()
				.registerTypeAdapter(BigDecimalAdapterTest.class,
						new NumberValueAdapter<>(BigDecimalAdapterTest::fromNumber))
				.registerTypeAdapter(LongAdapterTest.class, new NumberValueAdapter<>(LongAdapterTest::fromNumber))
				.registerTypeAdapter(IntegerAdapterTest.class, new NumberValueAdapter<>(IntegerAdapterTest::fromNumber))
				.create();
	}

	@Test
	void serializeBigDecimal() {
		BigDecimal bigDecimalValue = new BigDecimal(13000.98).setScale(2, RoundingMode.HALF_UP);
		BigDecimalAdapterTest bigDecimalAdapterTest = BigDecimalAdapterTest.of(bigDecimalValue);
		String actualJsonValue = gson.toJson(bigDecimalAdapterTest);
		String expectedJsonValue = String.format("%s", bigDecimalValue);
		assertEquals(actualJsonValue, expectedJsonValue);
	}
	
	@Test
	void deserializeBigDecimal() {
		BigDecimal bigDecimalValue = new BigDecimal (13000.98).setScale(2, RoundingMode.HALF_UP);
    	String jsonValue = String.format("%s", bigDecimalValue);
    	BigDecimalAdapterTest actualValue = gson.fromJson(jsonValue, BigDecimalAdapterTest.class);
    	BigDecimalAdapterTest expectedValue = BigDecimalAdapterTest.of(bigDecimalValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
	}

	@Test
	void serializeLong() {
		Long longValue = 3000000L;
		LongAdapterTest longAdapterTest = LongAdapterTest.of(longValue);
		String actualJsonValue = gson.toJson(longAdapterTest);
		String expectedJsonValue = String.format("%s", longValue);
		assertEquals(actualJsonValue, expectedJsonValue);
	}
	
	@Test
	void deserializeLong() {
		Long longValue = 3000000L;
    	String jsonValue = String.format("%s", longValue);
    	LongAdapterTest actualValue = gson.fromJson(jsonValue, LongAdapterTest.class);
    	LongAdapterTest expectedValue = LongAdapterTest.of(longValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
	}
	
	@Test
	void serializeInteger() {
		Integer integerValue = 100;
    	IntegerAdapterTest integerAdapterTest = IntegerAdapterTest.of(integerValue);
    	String actualJsonValue = gson.toJson(integerAdapterTest);
    	String expectedJsonValue = String.format("%s", integerValue);
    	assertEquals(actualJsonValue, expectedJsonValue);
	}
	
	@Test
	void deserializeInteger() {
		Integer integerValue = 100;
    	String jsonValue = String.format("%s", integerValue);
    	IntegerAdapterTest actualValue = gson.fromJson(jsonValue, IntegerAdapterTest.class);
    	IntegerAdapterTest expectedValue = IntegerAdapterTest.of(integerValue);
    	assertEquals(actualValue.valueOf(), expectedValue.valueOf());
	}

}
