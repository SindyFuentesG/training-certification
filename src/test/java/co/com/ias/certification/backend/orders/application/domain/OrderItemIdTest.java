package co.com.ias.certification.backend.orders.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class OrderItemIdTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear id item de una orden validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(7L, 35L, 146L).map(orderItemId -> {
			String testOrderItemId = String.format("Debería ser valido para el id: %s ", orderItemId);
			Executable executable = () -> {
				ThrowingSupplier<OrderItemId> quantityThrowingSupplier = () -> OrderItemId.of(orderItemId);
				assertAll(() -> assertDoesNotThrow(quantityThrowingSupplier),
						() -> assertNotNull(quantityThrowingSupplier.get()),
						() -> assertEquals(quantityThrowingSupplier.get().getValue(), orderItemId));
			};
			return DynamicTest.dynamicTest(testOrderItemId, executable);
		});
	}

	@TestFactory
	@DisplayName("No debería crear id item de una orden")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(-9L, -32L, -110L).map(orderItemId -> {
			String testOrderItemId = String.format("Debería ser invalido para el id %s", orderItemId);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class, () -> OrderItemId.of(orderItemId)));
			};
			return DynamicTest.dynamicTest(testOrderItemId, executable);
		});
	}

	@Test
	@DisplayName("No debería crear id de ordenes para valores envíados como null")
	void itShouldNotPassNull() {
		Long orderItemId = null;
		assertThrows(NullPointerException.class, () -> OrderItemId.of(orderItemId));
	}

}
