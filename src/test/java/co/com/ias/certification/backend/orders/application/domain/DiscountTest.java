package co.com.ias.certification.backend.orders.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class DiscountTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear descuentos validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(new BigDecimal(35000.46), new BigDecimal(5000), new BigDecimal(12000.14), new BigDecimal(0.0))
				.map(discount -> {
					String testDiscount = String.format("Debería ser valido para el descuento: %s ", discount);
					Executable executable = () -> {
						ThrowingSupplier<Discount> basePriceThrowingSupplier = () -> Discount.of(discount);
						assertAll(() -> assertDoesNotThrow(basePriceThrowingSupplier),
								() -> assertNotNull(basePriceThrowingSupplier.get()),
								() -> assertEquals(basePriceThrowingSupplier.get().getValue(), discount));
					};
					return DynamicTest.dynamicTest(testDiscount, executable);
				});
	}
	
	@TestFactory
	@DisplayName("No debería crear descuentos")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(
				new BigDecimal(-600.67), 
				new BigDecimal(-8900), 
				new BigDecimal(-45000.68)).map(discount -> {
					String testDiscount = String.format("Debería ser invalido para el descuento %s", discount);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> Discount.of(discount)));
					};
					return DynamicTest.dynamicTest(testDiscount, executable);
				});
	}
	
	@Test
	@DisplayName("No debería crear descuentos para valores envíados como null")
	void itShouldNotPassNull() {
		BigDecimal discount = null;
		assertThrows(NullPointerException.class, () -> Discount.of(discount));
	}


}
