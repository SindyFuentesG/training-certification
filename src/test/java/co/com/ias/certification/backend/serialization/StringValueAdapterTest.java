package co.com.ias.certification.backend.serialization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Value;

class StringValueAdapterTest {

	static Gson gson;

	@Test
	void of() {
	}

	@Value(staticConstructor = "of")
	public static class StringAdapterTest implements StringSerializable {
		String value;

		@Override
		public String valueOf() {
			return value;
		}
	}

	@BeforeAll
	static void setUp() {
		gson = new GsonBuilder()
				.registerTypeAdapter(StringAdapterTest.class, new StringValueAdapter<>(StringAdapterTest::of)).create();
	}

	@Test
	void serialize() {
		String stringvalue = "StringValueAdapterTest";
		StringAdapterTest stringAdapterTest = StringAdapterTest.of(stringvalue);
		String actualJsonValue = gson.toJson(stringAdapterTest);
		String expectedJsonValue = String.format("\"%s\"", stringvalue);
		assertEquals(actualJsonValue, expectedJsonValue);
	}

	@Test
	void deserialize() {
		String stringvalue = "StringValueAdapterTest";
		String jsonValue = String.format("\"%s\"", stringvalue);
		StringAdapterTest actualValue = gson.fromJson(jsonValue, StringAdapterTest.class);
		StringAdapterTest expectedValue = StringAdapterTest.of(stringvalue);
		assertEquals(actualValue.valueOf(), expectedValue.valueOf());
	}

}
