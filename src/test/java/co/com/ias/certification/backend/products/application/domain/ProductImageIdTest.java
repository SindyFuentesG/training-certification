package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class ProductImageIdTest {
	
	@Test
	void of() {
	}
	
	@TestFactory
	@DisplayName("Debería crear id de imagenes validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(5L, 68L, 42L).map(productImageId -> {
			String testProductImageId = String.format("Debería ser valido para el id: %s ", productImageId);
			Executable executable = () -> {
				ThrowingSupplier<ProductImageId> quantityThrowingSupplier = () -> ProductImageId
						.of(productImageId);
				assertAll(() -> assertDoesNotThrow(quantityThrowingSupplier),
						() -> assertNotNull(quantityThrowingSupplier.get()),
						() -> assertEquals(quantityThrowingSupplier.get().getValue(), productImageId));
			};
			return DynamicTest.dynamicTest(testProductImageId, executable);
		});
	}
	
	@TestFactory
	@DisplayName("No debería crear id de imagenes")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(-8L, -47L, -67L).map(productImageId -> {
			String testProductImageId = String.format("Debería ser invalido para el id %s", productImageId);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class,
						() -> ProductImageId.of(productImageId)));
			};
			return DynamicTest.dynamicTest(testProductImageId, executable);
		});
	}

	@Test
	@DisplayName("No debería crear id de imagenes para valores envíados como null")
	void itShouldNotPassNull() {
		Long productImageId = null;
		assertThrows(NullPointerException.class, () -> ProductImageId.of(productImageId));
	}


}
