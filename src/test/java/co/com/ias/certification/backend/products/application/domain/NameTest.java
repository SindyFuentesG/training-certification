package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class NameTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear nombres de productos validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of("Computador", "Teclado", "Mouse").map(name -> {
			String testName = String.format("Debería ser valido para el nombre: %s ", name);
			Executable executable = () -> {
				ThrowingSupplier<Name> nameThrowingSupplier = () -> Name.of(name);
				assertAll(() -> assertDoesNotThrow(nameThrowingSupplier),
						() -> assertNotNull(nameThrowingSupplier.get()),
						() -> assertEquals(nameThrowingSupplier.get().getValue(), name));
			};
			return DynamicTest.dynamicTest(testName, executable);
		});
	}

	@TestFactory
	@DisplayName("No debería crear nombres de producto")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of("",
				"Texto de prueba para nombre de productos muy largos, Texto de prueba para nombre de productos muy largos",
				"    ").map(name -> {
					String testName = String.format("Debería ser invalido para el name %s", name);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> Name.of(name)));
					};
					return DynamicTest.dynamicTest(testName, executable);
				});
	}

	@Test
	@DisplayName("No debería crear nombres de productos para valores envíados como null")
	void itShouldNotPassNull() {
		String name = null;
		assertThrows(NullPointerException.class, () -> Name.of(name));
	}

}
