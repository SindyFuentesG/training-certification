package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class ProductIdTest {
	
	@Test
	void of() {
	}
	
	@TestFactory
	@DisplayName("Debería crear id de productos validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(1L, 30L, 83L).map(productId -> {
			String testProductId = String.format("Debería ser valido para el id: %s ", productId);
			Executable executable = () -> {
				ThrowingSupplier<ProductId> quantityThrowingSupplier = () -> ProductId
						.of(productId);
				assertAll(() -> assertDoesNotThrow(quantityThrowingSupplier),
						() -> assertNotNull(quantityThrowingSupplier.get()),
						() -> assertEquals(quantityThrowingSupplier.get().getValue(), productId));
			};
			return DynamicTest.dynamicTest(testProductId, executable);
		});
	}
	
	@TestFactory
	@DisplayName("No debería crear id de productos")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(-1L, -23L, -127L).map(productId -> {
			String testProductId = String.format("Debería ser invalido para el id %s", productId);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class,
						() -> ProductId.of(productId)));
			};
			return DynamicTest.dynamicTest(testProductId, executable);
		});
	}

	@Test
	@DisplayName("No debería crear id de productos para valores envíados como null")
	void itShouldNotPassNull() {
		Long productId = null;
		assertThrows(NullPointerException.class, () -> ProductId.of(productId));
	}

}
