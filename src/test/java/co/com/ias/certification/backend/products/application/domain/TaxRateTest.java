package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class TaxRateTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear tasas de impuesto validas")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(new BigDecimal(0), new BigDecimal(1), new BigDecimal(0.5), new BigDecimal(0.8))
				.map(taxRate -> {
					String testTaxRate = String.format("Debería ser valido para la tasa de impuesto: %s ", taxRate);
					Executable executable = () -> {
						ThrowingSupplier<TaxRate> taxRateThrowingSupplier = () -> TaxRate.of(taxRate);
						assertAll(() -> assertDoesNotThrow(taxRateThrowingSupplier),
								() -> assertNotNull(taxRateThrowingSupplier.get()),
								() -> assertEquals(taxRateThrowingSupplier.get().getValue(), taxRate));
					};
					return DynamicTest.dynamicTest(testTaxRate, executable);
				});
	}

	@TestFactory
	@DisplayName("No debería crear tasas de impuesto")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(new BigDecimal(1.3), new BigDecimal(-2.5), new BigDecimal(4.6)).map(taxRate -> {
			String testTaxRate = String.format("Debería ser invalido para la tasa de impuesto %s", taxRate);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class, () -> TaxRate.of(taxRate)));
			};
			return DynamicTest.dynamicTest(testTaxRate, executable);
		});
	}

	@Test
	@DisplayName("No debería crear tasas de impuesto para valores envíados como null")
	void itShouldNotPassNull() {
		BigDecimal taxRate = null;
		assertThrows(NullPointerException.class, () -> TaxRate.of(taxRate));
	}

}
