package co.com.ias.certification.backend.orders.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class CustomerIdTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear id de clientes validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of("admin", "pepito", "juan").map(customerId -> {
			String testCustomerId = String.format("Debería ser valido para los id de clientes: %s ", customerId);
			Executable executable = () -> {
				ThrowingSupplier<CustomerId> nameThrowingSupplier = () -> CustomerId.of(customerId);
				assertAll(() -> assertDoesNotThrow(nameThrowingSupplier),
						() -> assertNotNull(nameThrowingSupplier.get()),
						() -> assertEquals(nameThrowingSupplier.get().getValue(), customerId));
			};
			return DynamicTest.dynamicTest(testCustomerId, executable);
		});
	}
	
	@TestFactory
	@DisplayName("No debería crear id de clientes")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of("",
				"    ").map(customerId -> {
					String testCustomerId = String.format("Debería ser invalido para los id de clientes %s", customerId);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> CustomerId.of(customerId)));
					};
					return DynamicTest.dynamicTest(testCustomerId, executable);
				});
	}

	@Test
	@DisplayName("No debería crear id de clientes para valores envíados como null")
	void itShouldNotPassNull() {
		String customerId = null;
		assertThrows(NullPointerException.class, () -> CustomerId.of(customerId));
	}

}
