package co.com.ias.certification.backend.orders.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class OrderIdTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear id de ordernes validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(5L, 45L, 100L).map(orderId -> {
			String testOrderId = String.format("Debería ser valido para el id: %s ", orderId);
			Executable executable = () -> {
				ThrowingSupplier<OrderId> quantityThrowingSupplier = () -> OrderId.of(orderId);
				assertAll(() -> assertDoesNotThrow(quantityThrowingSupplier),
						() -> assertNotNull(quantityThrowingSupplier.get()),
						() -> assertEquals(quantityThrowingSupplier.get().getValue(), orderId));
			};
			return DynamicTest.dynamicTest(testOrderId, executable);
		});
	}
	
	@TestFactory
	@DisplayName("No debería crear id de ordenes")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(-3L, -28L, -245L).map(orderId -> {
			String testOrderId = String.format("Debería ser invalido para el id %s", orderId);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class,
						() -> OrderId.of(orderId)));
			};
			return DynamicTest.dynamicTest(testOrderId, executable);
		});
	}
	
	@Test
	@DisplayName("No debería crear id de ordenes para valores envíados como null")
	void itShouldNotPassNull() {
		Long orderId = null;
		assertThrows(NullPointerException.class, () -> OrderId.of(orderId));
	}

}
