package co.com.ias.certification.backend.orders.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class TotalTest {
	
	@Test
	void of() {
	}
	
	@TestFactory
	@DisplayName("Debería crear totales validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(new BigDecimal(89000.79), new BigDecimal(50000), new BigDecimal(120000), new BigDecimal(0.0))
				.map(total -> {
					String testTotal = String.format("Debería ser valido para el total: %s ", total);
					Executable executable = () -> {
						ThrowingSupplier<Total> basePriceThrowingSupplier = () -> Total.of(total);
						assertAll(() -> assertDoesNotThrow(basePriceThrowingSupplier),
								() -> assertNotNull(basePriceThrowingSupplier.get()),
								() -> assertEquals(basePriceThrowingSupplier.get().getValue(), total));
					};
					return DynamicTest.dynamicTest(testTotal, executable);
				});
	}
	
	@TestFactory
	@DisplayName("No debería crear totales")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(
				new BigDecimal(-78000), 
				new BigDecimal(-65000), 
				new BigDecimal(-53000.93)).map(total -> {
					String testTotal = String.format("Debería ser invalido para el total %s", total);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> Total.of(total)));
					};
					return DynamicTest.dynamicTest(testTotal, executable);
				});
	}
	
	@Test
	@DisplayName("No debería crear totales para valores envíados como null")
	void itShouldNotPassNull() {
		BigDecimal total = null;
		assertThrows(NullPointerException.class, () -> Total.of(total));
	}

}
