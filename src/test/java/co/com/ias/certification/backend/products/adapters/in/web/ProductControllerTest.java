package co.com.ias.certification.backend.products.adapters.in.web;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import co.com.ias.certification.backend.products.application.port.in.CreateProductUseCase;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CreateProductUseCase createProductUseCase;
	
	@Autowired
	private Gson gson;
	
	@Test
	void findProduct() throws Exception {
		MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/all");
		this.mockMvc.perform(servletRequestBuilder).andDo(print()).andExpect(status().isOk())
				.andExpect(content().json("Hola"));
	
	}

}
