package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class InventoryQuantityTest {

	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear cantidades en inventario validas")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(1, 200, 3000).map(inventoryQuantity -> {
			String testQuantity = String.format("Debería ser valido para la cantidad: %s ", inventoryQuantity);
			Executable executable = () -> {
				ThrowingSupplier<InventoryQuantity> quantityThrowingSupplier = () -> InventoryQuantity
						.of(inventoryQuantity);
				assertAll(() -> assertDoesNotThrow(quantityThrowingSupplier),
						() -> assertNotNull(quantityThrowingSupplier.get()),
						() -> assertEquals(quantityThrowingSupplier.get().getValue(), inventoryQuantity));
			};
			return DynamicTest.dynamicTest(testQuantity, executable);
		});
	}

	@TestFactory
	@DisplayName("No debería crear cantidades en inventario")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(-1, -200, -4000).map(inventoryQuantity -> {
			String testQuantity = String.format("Debería ser invalido para la cantidad %s", inventoryQuantity);
			Executable executable = () -> {
				assertAll(() -> assertThrows(IllegalArgumentException.class,
						() -> InventoryQuantity.of(inventoryQuantity)));
			};
			return DynamicTest.dynamicTest(testQuantity, executable);
		});
	}

	@Test
	@DisplayName("No debería crear cantidades en inventario para valores envíados como null")
	void itShouldNotPassNull() {
		Integer inventoryQuantity = null;
		assertThrows(NullPointerException.class, () -> InventoryQuantity.of(inventoryQuantity));
	}

}
