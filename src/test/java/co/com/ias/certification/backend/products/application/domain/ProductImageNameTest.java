package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class ProductImageNameTest {
	
	@Test
	void of() {
	}

	@TestFactory
	@DisplayName("Debería crear nombres de imagenes validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of("ImagenPC", "ImagenTeclado", "ImageMouse").map(nameImage -> {
			String testNameImage = String.format("Debería ser valido para el nombre: %s ", nameImage);
			Executable executable = () -> {
				ThrowingSupplier<ProductImageName> nameThrowingSupplier = () -> ProductImageName.of(nameImage);
				assertAll(() -> assertDoesNotThrow(nameThrowingSupplier),
						() -> assertNotNull(nameThrowingSupplier.get()),
						() -> assertEquals(nameThrowingSupplier.get().getValue(), nameImage));
			};
			return DynamicTest.dynamicTest(testNameImage, executable);
		});
	}

	@TestFactory
	@DisplayName("No debería crear nombres de imagenes")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of("",
				"    ").map(name -> {
					String testName = String.format("Debería ser invalido para el nombre %s", name);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> ProductImageName.of(name)));
					};
					return DynamicTest.dynamicTest(testName, executable);
				});
	}

	@Test
	@DisplayName("No debería crear nombres de imagenes para valores envíados como null")
	void itShouldNotPassNull() {
		String imageName = null;
		assertThrows(NullPointerException.class, () -> ProductImageName.of(imageName));
	}


}
