package co.com.ias.certification.backend.products.application.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

class BasePriceTest {
	
	@Test
	void of() {
	}
	
	@TestFactory
	@DisplayName("Debería crear precios base validos")
	Stream<DynamicTest> itShouldPass() {
		return Stream.of(
				new BigDecimal(20000.87), 
				new BigDecimal(40000), 
				new BigDecimal(60000.146),
				new BigDecimal(0.0)).map(basePrice -> {
			String testBasePrice = String.format("Debería ser valido para el precio base: %s ", basePrice);
			Executable executable = () -> {
				ThrowingSupplier<BasePrice> basePriceThrowingSupplier = () -> BasePrice.of(basePrice);
				assertAll(() -> assertDoesNotThrow(basePriceThrowingSupplier),
						() -> assertNotNull(basePriceThrowingSupplier.get()),
						() -> assertEquals(basePriceThrowingSupplier.get().getValue(), basePrice));
			};
			return DynamicTest.dynamicTest(testBasePrice, executable);
		});
	}
	
	@TestFactory
	@DisplayName("No debería crear precios base")
	Stream<DynamicTest> itShouldNotPass() {
		return Stream.of(
				new BigDecimal(-100.46), 
				new BigDecimal(-400000), 
				new BigDecimal(-34000.97)).map(basePrice -> {
					String testBasePrice = String.format("Debería ser invalido para el precio base %s", basePrice);
					Executable executable = () -> {
						assertAll(() -> assertThrows(IllegalArgumentException.class, () -> BasePrice.of(basePrice)));
					};
					return DynamicTest.dynamicTest(testBasePrice, executable);
				});
	}
	
	@Test
	@DisplayName("No debería crear precios base para valores envíados como null")
	void itShouldNotPassNull() {
		BigDecimal basePrice = null;
		assertThrows(NullPointerException.class, () -> BasePrice.of(basePrice));
	}

}
